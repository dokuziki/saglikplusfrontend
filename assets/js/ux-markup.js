/**
 * Created by coskudemirhan on 15/04/2017.
 */
$(document).ready(function () {
    var option = $('[ux-init]').attr('ux-init');

    var propertyList = [
        'color',
        'width',
        'height','max-height',
        'float',
        'padding', 'padding-top', 'padding-bottom', 'padding-left', 'padding-right',
        'margin', 'margin-top', 'margin-bottom', 'margin-left', 'margin-right',
        'background','background-image','background-color',
        'font-size','font-size','font-weight','line-height','text-align',
        'display',
        'border', 'border-top', 'border-bottom', 'border-left', 'border-right',
        'border-radius',
        'overflow','overflow-y','overflow-x',
        'position',
        'z-index',
    ];

    for (i in propertyList) {
        var key = propertyList[i];

        $('*[ux-' + key + ']').each(function (i, item) {
            var item = $(item);
            item.css(key, item.attr('ux-' + key));
        });
    }


    /*
     * Active Fill
     */

    $("*[ux-active-fill]").each(function (i, item) {
        var item = $(item);
        var val = item.attr('ux-active-fill');


        if (val === 'height') {
            item.css('height', item.parent().height());
        }

        if (val === 'true' || val === 'width' ) {
            item.css('width', item.parent().width());
        }
    });


    $("*[ux-active-contain]").each(function (i, item) {
        var item = $(item);
        var val = item.attr('ux-active-contain');

        if(val === true || val === "true"){
            item.css('height', $('body').innerHeight() - item.offset().top - 30);
        }

        if(val === 'header'){
            item.css('height', $(window).innerHeight() - 64);
        }
    });

    $('body').css('opacity',1);

});